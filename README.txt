INTRODCTION
-----------
"MIME file validator" validates file type on server side. This is simple module. In drupal 7, files validate only on file name extension. This module will check MIME file type and validate accordingly. 

DEPENDENCIES
------------
This module is dependent on "fileinfo" php library. Please make sure that fileinfo is enabled on server.

INSTALLATION
------------
Install module like usual.

CONFIGURATION
------------- 
There is no configuration option in admin section. After normal installation, It will start working automatically.
